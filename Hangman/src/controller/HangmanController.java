package controller;

import apptemplate.AppTemplate;
import data.GameData;
import data.GameDataFile;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {
    private static AppTemplate appTemplate; // shared reference to the application
    private static GameData gamedata;    // shared reference to the game being played, loaded or saved
    private Text[] progress = new Text[0];    // reference to the text area for the word
    private boolean success;     // whether or not player was successful
    private int discovered;  // the number of letters already discovered
    private Button gameButton;  // shared reference to the "start game" button
    private Label remains;     // dynamically updated label that indicates the number of remaining guesses
    private boolean gameover;    // whether or not the current game is already over
    private static boolean savable;
    private Path workFile;
    private Path loadPath; //replicate our workFile for reference
    private HBox remainingGuessBox;
    private HBox guessedLetters;
    private Workspace gameWorkspace;
    private GameDataFile gdf; //gamedatafile for stuff like getting back set of the loaded data
    public Set<Character> bGuesses = new HashSet<Character>();
    public Set<Character> gGuesses = new HashSet<Character>();

    private static boolean proceedLoad;
    private static boolean proceedNew;
    private static boolean proceedExit;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void start() {
        gamedata = new GameData(appTemplate);
        gameover = false;
        success = false;
        discovered = 0;
        gameButton.setDisable(true);
        workFile = null;
        savable = true;
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);
        play();
    }

    private void end() {
        proceedExit = false;
        proceedLoad = false;
        proceedNew = false;
        AppMessageDialogSingleton youwinyoulose = AppMessageDialogSingleton.getSingleton();
        if(success) {
            youwinyoulose.show2("You win!", "Congratulations! You win, like, $5,000,000 or whatever.");
            youwinyoulose.setButtonLabel(true);
        }
        else {
            youwinyoulose.show2("You lose!","Ah, close but not quite. The word is " +
                    "\"" + gamedata.getTargetWord() + "\".");
            youwinyoulose.setButtonLabel(false);
        }

        remains.setText(gamedata.getRemainingGuesses() + "");
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(true);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        guessedLetters.getChildren().addAll(progress);
    }


    private void initWordGraphics(HBox guessedLetters, Text[]progress) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        for (int i = 0; i < progress.length; i++) {
            progress[i].setText(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        guessedLetters.getChildren().addAll(progress);
    }

    public void play() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);

                    if (Character.isLetter(guess)){
                        guess = Character.toLowerCase(guess);
                    }
                    else if (guess == '\''){
                        guess = '\'';
                    }
                    if (Character.isLetter(guess) || guess == '\'') {

                        if (!alreadyGuessed(guess)) {
                            boolean goodguess = false;
                            for (int i = 0; i < progress.length; i++) {
                                if (gamedata.getTargetWord().charAt(i) == guess) {
                                    savable = true;
                                    appTemplate.getGUI().updateWorkspaceToolbar(savable);
                                    progress[i].setVisible(true);
                                    gamedata.addGoodGuess(guess);
                                    goodguess = true;
                                    discovered++;
                                }
                            }
                            if (!goodguess) {
                                savable = true;
                                appTemplate.getGUI().updateWorkspaceToolbar(savable);
                                gamedata.addBadGuess(guess);
                            }
                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                        }
                    }

                });

                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();
        boolean makenew = true;
        if (savable)
            try {
                makenew = promptToSave("new");
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

        if (gameover) {
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }

    //save functionality kinda works, just need to figure out how to store .json data and stuff like that
    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        try {
            if (workFile != null)
                save(workFile);
            else {

//                this links the URL thing to point to the "saved" folder underneath resources ... or so it should...
//                can't get it to work properly, but since we're saving it anyway, i guess it doesn't matter anyway
                URL workDirURL = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
                if (workDirURL == null) {

                    throw new FileNotFoundException("Work folder not found under resources.");
                }
                 else {
//
        File initialDir = new File(workDirURL.getFile());
                FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(initialDir);
                fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));

//                  went to the xml file and fixed WORK_FILE_EXT -- changed json to .json, now files save properly
                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
                        propertyManager.getPropertyValue(WORK_FILE_EXT)));

                File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null){
                    while(selectedFile.getName().length() < 6 ||
                            !selectedFile.getName().substring(selectedFile.getName().length() - 5).equals(".json")
                            ){
                        AppMessageDialogSingleton aaa = AppMessageDialogSingleton.getSingleton();
                        aaa.setButtonLabel(false, false, false);
                        aaa.show("Incorrect-o Format-o!", "Please, only save as .json file.");

                        selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
                    }
                    save(selectedFile.toPath());
                }
                }
            }
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    //added code here
    private void save(Path target) throws IOException {
        workFile = target;
        appTemplate.getFileComponent()
                .saveData(gamedata, Paths.get(workFile.toFile().getAbsolutePath()));

        //researched how to use the .json library and looked up the documentations
        //now i can successfully write .json data into .json files into wherever i want

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager props = PropertyManager.getManager();

        dialog.setButtonLabel(proceedNew, proceedLoad, proceedExit);

        proceedNew = false;
        proceedLoad = false;
        proceedExit = false;

        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));

        savable = false;
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
    }

    @Override
    public void handleLoadRequest() throws IOException {

        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();
        boolean continueLoad = true;
        if (savable) {
            try {
                continueLoad = promptToSave("load");
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(LOAD_ERROR_TITLE), propertyManager.getPropertyValue(LOAD_ERROR_MESSAGE));
            }
        }
            if (continueLoad) {
                FileChooser load = new FileChooser();
                File selectedFile = load.showOpenDialog(appTemplate.getGUI().getWindow());
                while(selectedFile.getName().length() < 6 ||
                !
                selectedFile.getName().substring(selectedFile.getName().length() - 5).equals(".json")){
                    AppMessageDialogSingleton aaa = AppMessageDialogSingleton.getSingleton();
                    aaa.setButtonLabel(false, false, false);
                    aaa.show("Incorrect-o File-o", "Please select a .json file only.");

                    selectedFile = load.showOpenDialog(appTemplate.getGUI().getWindow());
                }
                if (selectedFile != null) {
                    gdf = new GameDataFile();
                    if(gamedata != null) {
                        gdf.loadData(gamedata, selectedFile.toPath());
                    }
                    else {
                        gamedata = new GameData(appTemplate);
                        gdf.loadData(gamedata, selectedFile.toPath());
                    }

                    workFile = selectedFile.toPath();
                    loadPath = workFile;
                    load(); //is a helper method for handleLoadRequest()
                }
            }

        }

    private void load() {

        ArrayList<String> b = gdf.getBadList();
        ArrayList<String> g = gdf.getGoodList();

        String word = gamedata.getTargetWord();
        char[] targetword = word.toCharArray();
        int remainingGuesses = gamedata.getRemainingGuesses();
        Set<Character> bad = gamedata.getBadGuesses();
        Set<Character> good = gamedata.getGoodGuesses();

        savable = false; // just reloaded a saved game, pointless to load it back as savable
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

        handleNewRequest();
        startLoad();

        gamedata = new GameData(appTemplate);
        gamedata.reset();
        gamedata.setTargetWord(word);
        gamedata.setBadGuesses(bad);
        gamedata.setGoodGuesses(good);
        gamedata.setRemainingGuesses(remainingGuesses);

        progress = new Text[word.length()];

        for (int i = 0; i < targetword.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }

        discovered = 0;

        for (int z = 0; z < g.size(); z++) {
            for (int j = 0; j < word.length(); j++) {
                if ((word.charAt(j)+"").equals(g.get(z))) {
                    progress[j] = new Text(g.get(z));
                    progress[j].setVisible(true);
                    discovered++;
                }
            }
        }

        workFile = loadPath;
        savable = false;
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

        remains = new Label(Integer.toString(remainingGuesses));
        remainingGuessBox.getChildren().setAll(new Label("Remaining Guesses: "), remains);

        guessedLetters.getChildren().setAll(progress);

        play();

    }

    public void startLoad() {
        gamedata = new GameData(appTemplate);
        gameover = false;
        success = false;
        gameButton.setDisable(true);

        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (savable) {
                exit = promptToSave("exit");
            }
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }


    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    //added code in here
    private boolean promptToSave(String proceedS) throws IOException {
        File selectedFile = null;
        PropertyManager propertyManager = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            if(proceedS.equals("new")){
                proceedNew = true;
            }
            else if(proceedS.equals("load")){
                proceedLoad = true;
            }
            else if(proceedS.equals("exit")){
                proceedExit = true;
            }
            if (workFile != null)
                save(workFile);
            else {
                FileChooser filechooser = new FileChooser();
                URL workDirURL = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
                if (workDirURL == null)
                    throw new FileNotFoundException("Work folder not found under resources.");

                File initialDir = new File(workDirURL.getFile());
                filechooser.setInitialDirectory(initialDir);
                filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));

                String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
                String extension = propertyManager.getPropertyValue(WORK_FILE_EXT);

                FileChooser.ExtensionFilter extFilter = (new FileChooser.ExtensionFilter(description,
                        extension));
                filechooser.getExtensionFilters().add(extFilter);

                selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null)
                    save(selectedFile.toPath());
                if(selectedFile == null) {
                        proceedNew = false;
                        proceedLoad = false;
                        proceedExit = false;


                    return yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
                }
            }

        }

        if(yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL)){
            proceedNew = false;
            proceedLoad = false;
            proceedExit = false;
        }
        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
}
