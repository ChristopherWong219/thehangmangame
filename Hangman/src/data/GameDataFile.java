package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Ritwik Banerjee
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES = "BAD_GUESSES";

    public String word; // now we can effectively save data of the target word from the load file to use
    // in our HangmanController

    public int remainingG;
    public ArrayList<String> badGuesses = new ArrayList<String>();
    public ArrayList<String> goodGuesses = new ArrayList<String>();
    public Set<Character> bGuesses = new HashSet<Character>();
    public Set<Character> gGuesses = new HashSet<Character>();
    public GameData gameData;

    @Override
    public void saveData(AppDataComponent data, Path to) throws IOException {
        if (data instanceof GameData) {

            Set<Character> badGuesses = ((GameData) data).getBadGuesses();
            Set<Character> goodGuesses = ((GameData) data).getGoodGuesses();

            JsonFactory factory = new JsonFactory();

            JsonGenerator generator = factory.createGenerator(
                    to.toFile(), JsonEncoding.UTF8);

            generator.writeStartObject();
            generator.writeStringField("word", ((GameData) data).getTargetWord());
            generator.writeEndObject();
            generator.writeStartObject();
            generator.writeNumberField("remainingG", ((GameData) data).getRemainingGuesses());
            generator.writeEndObject();


            for (char b : badGuesses) {
                generator.writeStartObject();
                generator.writeStringField("badGuess", b + "");
                generator.writeEndObject();
            }


            for (char g : goodGuesses) {
                generator.writeStartObject();
                generator.writeStringField("goodGuess", g + "");
                generator.writeEndObject();
            }


            generator.close();
        }
    }

    @Override
    public void loadData(AppDataComponent gamedata, Path from) throws IOException {

        JsonFactory factory = new JsonFactory();
        JsonParser jp = factory.createParser(
                from.toFile());


        while (jp.nextToken() != null) {
            JsonToken jsonToken = jp.nextToken();
            if (JsonToken.FIELD_NAME.equals(jsonToken)) {
                String fieldName = jp.getCurrentName();
                JsonToken valueName = jp.nextToken();
                if (fieldName.equals("word")) {
                    word = jp.getValueAsString();
                }
                if (fieldName.equals("remainingG")) {
                    remainingG = jp.getValueAsInt();
                }
                if (fieldName.equals("badGuess")) {
                    String bad = jp.getValueAsString();
                    badGuesses.add(bad);
                }
                if (fieldName.equals("goodGuess")) {
                    String good = jp.getValueAsString();
                    goodGuesses.add(good);

                }
            }
            jp.nextToken();
        }

        initializeBg();
        initializeGg();
        gameData = (GameData) gamedata;
        gameData.setBadGuesses(bGuesses);
        gameData.setGoodGuesses(gGuesses);
        gameData.setTargetWord(word);
        gameData.setRemainingGuesses(remainingG);

    }

    public String getWord() {
        return gameData.getTargetWord();
    }

    public Set<Character> getbadGuesses() {
        return gameData.getBadGuesses();
    }

    public Set<Character> getgoodGuesses() {
        return gameData.getGoodGuesses();
    }

    public int getremainingGuesses() {
        return gameData.getRemainingGuesses();
    }

    private void initializeBg() {
        for (String b : badGuesses) {
            bGuesses.add(b.charAt(0));
        }
    }

    private void initializeGg() {
        for (String g : goodGuesses) {
            gGuesses.add(g.charAt(0));
        }
    }

    public ArrayList<String> getBadList() {
        return badGuesses;
    }

    public ArrayList<String> getGoodList() {
        return goodGuesses;
    }

    /**
     * This method will be used if we need to export data into other formats.
     */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {
    }
}
