package settings;

/**
 * @author Ritwik Banerjee
 */
public enum InitializationParameters {

    APP_PROPERTIES_XML("app-properties.xml"),
    WORKSPACE_PROPERTIES_XML("workspace-properties.xml"),
    PROPERTIES_SCHEMA_XSD("properties-schema.xsd"),
    ERROR_DIALOG_BUTTON_LABEL("Thanks for Playing"),
    LOAD_DIALOG_BUTTON_LABEL("Proceed to Load Game"),
    SAVE_DIALOG_BUTTON_LABEL("Continue"),
    NEW_DIALOG_BUTTON_LABEL("Proceed to New Game"),
    APP_WORKDIR_PATH("saved"),
    APP_IMAGEDIR_PATH("images");

    private String parameter;

    InitializationParameters(String parameter) {
        this.parameter = parameter;
    }

    public String getParameter() {
        return parameter;
    }
}